<?php

namespace Tank;

require_once('Unit.php');
use unit\Unit;

class Tank extends Unit
{
    public function move()
    {
        return 'move tank';
    }

    public function shoot()
    {
        return 'shoot tank';
    }
}