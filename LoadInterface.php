<?php


namespace loadInterface;


interface LoadInterface
{
    public function load();

    public function unload();

}