<?php

namespace Bmp;

require_once 'LoadInterface.php';

use unit\Unit;
use loadInterface\LoadInterface;
use Exception;

class Bmp extends Unit
{
    private $maxCount = 40;
    private $count = 0;

    public function move()
    {
        return 'move bmp';
    }

    public function shoot()
    {
        return 'bmp shoot';
    }

    public function loadingPeople(LoadInterface $load)
    {
        if (!$this->isFreePlace()){
           throw new Exception('Not free place');
        }
        $this->count += $load->load();
        return $this;
    }

    private function isFreePlace()
    {
        if ($this->count < $this->maxCount){
            return true;
        }
        return false;
    }

    public function unloadingPeople(LoadInterface $load)
    {
        if(!isset($this->count) && $this->count <= $load->load()){
           throw new Exception('Not found get count people');
        }
        $this->count -= $load->load();
        return $this;
    }

    public function getCountInBmp()
    {
        return $this->count;
    }

}