<?php

namespace infantry;

require_once 'Unit.php';
require_once 'LoadInterface.php';

use loadInterface\LoadInterface;
use unit\Unit;

class Infantry extends Unit implements LoadInterface
{
    public $countPeople = 0;

    public function move()
    {
        return 'move infantry';
    }

    public function shoot()
    {
        return 'shoot infantry';
    }

    public function load()
    {
        return $this->countPeople;
    }

    public function unload()
    {
        return $this->countPeople;
    }
}