<?php
namespace unit;

abstract class Unit
{
    abstract public function move();

    abstract public function shoot();
}